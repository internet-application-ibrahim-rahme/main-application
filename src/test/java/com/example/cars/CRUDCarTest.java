package com.example.cars;

import com.example.cars.dto.CarDto;
import com.example.cars.dto.LoginDto;
import com.example.cars.dto.UserRegister;
import com.example.cars.entities.Car;
import com.example.cars.entities.Parameter;
import com.example.cars.repositories.CarRepository;
import com.example.cars.repositories.ParameterRepo;
import com.example.cars.services.CarService;
import com.example.cars.services.ParameterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.when;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// to run all the context (model,view,controller)
@SpringBootTest
@AutoConfigureMockMvc
class CRUDCarTest {

    @Autowired
    MockMvc mockMvc;


    @MockBean
    ParameterService parameterService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void testAll() throws Exception {
        when(parameterService.findBypKey("SEAT_COUNT")).thenReturn(Optional.of(new Parameter("SEAT_COUNT","4")));
        register();
        String token = login().split(" ")[1];
//        Car car1 = new Car("test", 3, 3);
//        car1.setVersion(0);
//        carRepository.save(car1);

        // post car with seat count
        CarDto car = new CarDto("post car", 100, 4);
        String postCarRoute = "/api/car/";
        this.mockMvc.perform(post(postCarRoute).header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car))).andDo(print()).andExpect(status().isOk());


        // get all cars
        String getAllCarsRoutes = "/api/car/";
        this.mockMvc.perform(get(getAllCarsRoutes).header("Authorization", "Bearer " + token)).andDo(print()).andExpect(status().isOk());


        // update car
        int updateId = 3;
        CarDto car2 = new CarDto("Update", 100, 4);
        String updateCarRoute = "/api/car/" + updateId + "";
        this.mockMvc.perform(patch(updateCarRoute).header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car2))).andDo(print()).andExpect(status().isOk());


        // post car with out seatCount
        CarDto car3 = new CarDto("post car 3", 100);
        this.mockMvc.perform(post(postCarRoute).header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(car3))).andDo(print()).andExpect(status().isOk());


        // delete car
        int deleteId = 3;
        String deleteCarRoute = "/api/car/" + deleteId + "";
        this.mockMvc.perform(delete(deleteCarRoute).header("Authorization", "Bearer " + token))
                .andDo(print()).andExpect(status().isOk());

    }

    void register() throws Exception {
        UserRegister login = new UserRegister("Bassel2", "123456");

        String register = "/auth/register";
        System.out.println(this.mockMvc.perform(post(register).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(login))).andReturn().getResponse().getContentAsString());
    }

    // Login and get token
    String login() throws Exception {
        LoginDto login = new LoginDto();
        login.username = "Bassel2";
        String password = "123456";
        login.password = password;

        String loginRoute = "/auth/login";
        return this.mockMvc.perform(post(loginRoute).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(login))).andReturn().getResponse().getContentAsString();
    }
}
