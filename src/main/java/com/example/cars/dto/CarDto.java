package com.example.cars.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CarDto {

    public String name;
    public Integer price;
    public Integer seatCount;

   public  CarDto(){}
    public CarDto(String name, Integer price, Integer seatCount) {
        this.name = name;
        this.price = price;
        this.seatCount = seatCount;
    }


    public CarDto(String name, Integer price) {
        this.name = name;
        this.price = price;
    }
}
