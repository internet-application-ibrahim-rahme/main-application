package com.example.cars.dto;

public class SellCarDto {

    public Integer sellPrice;
    public String buyerName;
    public Integer version;

    public SellCarDto(Integer sellPrice, String buyerName, Integer version) {
        this.sellPrice = sellPrice;
        this.buyerName = buyerName;
        this.version = version;
    }
}
