package com.example.cars.dto;

public class UserRegister {
   public String username;
   public String password;

   public UserRegister(String username, String password) {
      this.username = username;
      this.password = password;
   }
   public UserRegister(){}
}
