package com.example.cars.listeners;

import com.example.cars.dto.RequestReport;
import org.springframework.stereotype.Component;

@Component
public class RabbitMqReceiver {

    public void receiveMessage(RequestReport message) {
        //System.out.println("Received: " + message.content);
    }

}
