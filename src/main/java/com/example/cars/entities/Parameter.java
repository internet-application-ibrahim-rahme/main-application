package com.example.cars.entities;

import javax.persistence.*;

@Entity
public class Parameter {

    @Id
    @Column(nullable = false,unique = true)
    private String pKey;
    @Column(nullable = false)
    private String pValue;

    public Parameter(){}
    public Parameter(String key, String pValue) {
        this.pKey = key;
        this.pValue = pValue;
    }

    public String getKey() {
        return pKey;
    }

    public void setKey(String key) {
        this.pKey = key;
    }

    public String getpValue() {
        return pValue;
    }

    public void setpValue(String pValue) {
        this.pValue = pValue;
    }
}
