package com.example.cars.controllers;

import com.example.cars.configs.RabbitMq;
import com.example.cars.dto.RequestReport;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("/api/inventory")
public class InventoryController {

    private RabbitTemplate rabbitTemplate;

    InventoryController(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }

    @PostMapping("/report")
    public ResponseEntity<Object> requestReport(@RequestBody RequestReport req){
        if(req.date == null){
            req.date = new Date();
        }
        rabbitTemplate.convertAndSend(RabbitMq.topicExchangeName,"foo.bar.baz", req);

        return ResponseEntity.ok("Success");
    }

}
