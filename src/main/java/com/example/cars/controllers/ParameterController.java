package com.example.cars.controllers;

import com.example.cars.dto.CarDto;
import com.example.cars.dto.UpdateParameter;
import com.example.cars.entities.Car;
import com.example.cars.entities.Parameter;
import com.example.cars.services.CarService;
import com.example.cars.services.ParameterService;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/parameter")
public class ParameterController {

    private ParameterService parameterService;

    ParameterController(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Parameter> update(@PathVariable String id, @RequestBody UpdateParameter paramDto) {
        return parameterService.update(id,paramDto);
    }

}
