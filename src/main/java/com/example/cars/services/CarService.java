package com.example.cars.services;

import com.example.cars.dto.CarDto;
import com.example.cars.dto.SellCarDto;
import com.example.cars.entities.Car;
import com.example.cars.repositories.CarRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CarService {

    private CarRepository carRepository;
    private ParameterService parameterService;

    CarService(CarRepository carRepository, ParameterService parameterService) {
        this.carRepository = carRepository;
        this.parameterService = parameterService;
    }

    /**
     * Get all not sold Cars
     *
     * @return
     */
    public ResponseEntity<List<Car>> index() {

        return ResponseEntity.ok(carRepository.findBySaleDate(null));
    }

    /**
     * Store a new Care
     *
     * @param carDto
     * @return
     */
    public ResponseEntity<Car> store(CarDto carDto) {
        // 1- the the seat number if not present

        if (carDto.seatCount == null) {
            // get from the parameter
            carDto.seatCount = Integer.parseInt(parameterService.findBypKey("SEAT_COUNT").get().getpValue());
        }
        Car car = new Car(carDto.name, carDto.price, carDto.seatCount);
        car.setVersion(0);
        return ResponseEntity.ok(carRepository.save(car));
    }

    /**
     * Update an old car
     *
     * @param id
     * @param carDto
     * @return
     */
    public ResponseEntity<Car> update(Long id, CarDto carDto) {

        // 1- get the car
        Car car = new Car();
        try {
            car = carRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Car>(car, HttpStatus.NOT_FOUND);
        }
        // 2- update the car
        car.setName((carDto.name != null) ? carDto.name : car.getName());
        car.setPrice((carDto.price != null) ? carDto.price : car.getPrice());
        car.setSeatCount((carDto.seatCount != null) ? carDto.seatCount : car.getSeatCount());

        return ResponseEntity.ok(carRepository.save(car));
    }

    /**
     * Delete Car
     *
     * @param id
     * @return
     */
    public ResponseEntity<Car> delete(Long id) {
        // 1- get the car
        Car car = carRepository.findById(id).get();
        carRepository.deleteById(id);
        return ResponseEntity.ok(car);
    }

    /**
     * Sell old car
     *
     * @param carDto
     * @return
     */
    @Transactional
    public ResponseEntity<Object> sell(Long id, SellCarDto carDto) {
        // 1- get the car
        Car car = new Car();
        try {
            car = carRepository.findById(id).get();

        } catch (NoSuchElementException e) {
            return new ResponseEntity<Object>(car, HttpStatus.NOT_FOUND);
        }

        // 2- get the PROFIT_PERCENTAGE parameter
        if (carDto.sellPrice == null) {
            Integer temp = Integer.parseInt(parameterService.findBypKey("PROFIT_PERCENTAGE").get().getpValue());
            carDto.sellPrice = car.getPrice() + car.getPrice() * temp / 100;// original price 200 + 10% profit = 220
        }

        // 3- update  new  update methode
        int result = carRepository.updateCarSetSalePriceAndBuyerNameAndSaleDate(id, carDto.sellPrice, carDto.buyerName, new Date(), carDto.version, carDto.version + 1);
        if (result == 1) {

            return ResponseEntity.ok("Success");
        } else {
            return ResponseEntity.badRequest().body("Failed wrong version");
        }
    }

    /**
     * @param nameLike String
     * @return ResponseEntity<List < Car>>
     */
    public ResponseEntity<List<Car>> search(String nameLike) {
        return ResponseEntity.ok(carRepository.findByNameLike(nameLike));
    }


}
