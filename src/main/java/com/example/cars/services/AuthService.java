package com.example.cars.services;

import com.example.cars.dto.LoginDto;
import com.example.cars.dto.UserRegister;
import com.example.cars.entities.User;
import com.example.cars.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class AuthService {

    private UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AuthService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;

    }

    public ResponseEntity<User> register(UserRegister userdto) {
        var user = new User();
        user.setUsername(userdto.username);
        user.setPassword(bCryptPasswordEncoder.encode(userdto.password));
        userRepository.save(user);
        return ResponseEntity.ok(user);

    }

    public ResponseEntity<User> login(LoginDto loginDto) {
        var user = new User();
        user.setPassword("testing");
        user.setUsername("testing");
        return ResponseEntity.ok(user);
    }
}
