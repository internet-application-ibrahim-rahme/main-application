package com.example.cars.services;


import com.example.cars.dto.CarDto;
import com.example.cars.dto.UpdateParameter;
import com.example.cars.entities.Car;
import com.example.cars.entities.Parameter;
import com.example.cars.repositories.ParameterRepo;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@CacheConfig(cacheNames = "parameters")
public class ParameterService {

    private ParameterRepo parameterRepo;

    ParameterService(ParameterRepo parameterRepo){
        this.parameterRepo = parameterRepo;
    }

    /**
     * get parameter by key
     * Cache will update the cache so i dont remove too much with (cacheEvict) i just do update
     */
    @CachePut(value = "parameters")
    public Optional<Parameter> findBypKey(String key){
        return parameterRepo.findBypKey(key);
    }


    /**
     * Todo: add logs to it
     * update parameter
     * @param id {Long}
     * @param  carDto {CarDto}
     * @return
     */
    // @CacheEvict(value = "parameters")
    public ResponseEntity<Parameter> update(String id, UpdateParameter paramDto) {
        Parameter p = parameterRepo.findBypKey(id).get();
        p.setpValue(paramDto.value);
        parameterRepo.save(p);
        return ResponseEntity.ok(p);
    }
}
