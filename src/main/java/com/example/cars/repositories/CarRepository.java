package com.example.cars.repositories;

import com.example.cars.entities.Car;
import com.example.cars.entities.Parameter;
import org.hibernate.annotations.ParamDef;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends CrudRepository<Car,Long> {

    List<Car> findBySaleDate(Date sale_date);
    List<Car> findByNameLike(String nameLike);

    // sell car
    @Modifying
    @Query("update Car c set c.salePrice = :sellPrice , c.buyerName = :buyerName , c.saleDate = :saleDate , c.version = :version1 where c.id = :id and c.version = :version")
    int updateCarSetSalePriceAndBuyerNameAndSaleDate(@Param("id") Long id ,@Param("sellPrice") Integer sellPrice
            ,@Param("buyerName") String buyerName,@Param("saleDate") Date saleDate,@Param("version") Integer version
            ,@Param("version1") Integer version1);

}
