package com.example.cars.repositories;

import com.example.cars.entities.Logs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends CrudRepository<Logs, Integer> {


}
