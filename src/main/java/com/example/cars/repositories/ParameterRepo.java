package com.example.cars.repositories;

import com.example.cars.entities.Parameter;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParameterRepo extends CrudRepository<Parameter, String> {



    Optional<Parameter> findBypKey(String pKey);
}
