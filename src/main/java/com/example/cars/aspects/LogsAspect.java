package com.example.cars.aspects;

import com.example.cars.entities.Logs;
import com.example.cars.repositories.LogRepository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class LogsAspect {

    private Logger logger = LogManager.getLogger("LogsLogger");

    @Autowired
    private LogRepository logRepository;

    //&& args(name,other) and pass it to the function
    @Before(value = "execution(* com.example.cars.services.CarService*.index(..))")
    public void beforeIndex(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: Index, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Index");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Cars");
        logRepository.save(l);
    }

    //&& args(name,other) and pass it to the function
    @Before(value = "execution(* com.example.cars.services.CarService*.store(..))")
    public void beforeStore(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: store, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Store");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Cars");
        logRepository.save(l);
    }

    //&& args(name,other) and pass it to the function
    @Before(value = "execution(* com.example.cars.services.CarService*.update(..))")
    public void beforeUpdate(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: update, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Update");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Cars");
        logRepository.save(l);
    }

    //&& args(name,other) and pass it to the function
    @Before(value = "execution(* com.example.cars.services.CarService*.delete(..))")
    public void beforeDelete(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: delete, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Delete");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Cars");
        logRepository.save(l);
    }

    //&& args(name,other) and pass it to the function
    @Before(value = "execution(* com.example.cars.services.CarService*.sell(..))")
    public void beforeSell(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: sell, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Sell");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Cars");
        logRepository.save(l);
    }

    @Before(value = "execution(* com.example.cars.services.ParameterService*.update(..))")
    public void beforeParamterUpdate(JoinPoint joinPoint) {
        logger.log(Level.INFO, "Before method: update parameter, path: " + joinPoint.getSignature());
        Logs l = new Logs();
        l.setAction("Update");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        l.setActorId(auth.getPrincipal().toString()); l.setEntities("Parameter");
        logRepository.save(l);
    }
}
